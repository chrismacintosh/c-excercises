﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace C_Sharp_Drills
{
    class Program
    {
        static void overload(int x)
        {
            Console.WriteLine("\nOverloaded int" );
            Console.Write(x);
        }
        static void overload(double y)
        {
            Console.WriteLine("\nOverloaded double" );
            Console.Write(y);
        }
        static void overload(float z)
        {
            Console.WriteLine("\nOverloaded float" );
            Console.Write(z);
        }
        static void overload(string a)
        {
            Console.WriteLine("\nOverloaded string ");
            Console.Write(a);
        }
        static void overload(long b)
        {
            Console.WriteLine("\nOverloaded long ");
            Console.Write(b);
        }
        static void Main(string[] args)
        {
            try
            {
                long yargh = 3958235982359235;
                float yarghb = 3.14F;
                string yargbh = "String";
                double yargz = 2952935923592359235;
                int yarga = 32;

                overload(yargh);
                overload(yarghb);
                overload(yargbh);
                overload(yarga);
                overload(yargz);
            }
            finally
            {
                Console.WriteLine("Done!");
                //Leave a console.read to leave the window open for viewing
                Console.Read();
            }
        }
    }
}
