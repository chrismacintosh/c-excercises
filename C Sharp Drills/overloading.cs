﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace C_Sharp_Drills
{
    class drills
    {
        static void overload(int x)
        {
            Console.WriteLine("\nOverloaded int");
            Console.Write(x);
        }
        static void overload(double y)
        {
            Console.WriteLine("\nOverloaded double");
            Console.Write(y);
        }
        static void overload(float z)
        {
            Console.WriteLine("\nOverloaded float");
            Console.Write(z);
        }
        static void overload(string a)
        {
            Console.WriteLine("\nOverloaded string ");
            Console.Write(a);
        }
        static void overload(long b)
        {
            Console.WriteLine("\nOverloaded long ");
            Console.Write(b);
        }
        static void Main(string[] args)
        {
            try
            {
                long yargh = 3958235982359235;
                float yarghb = 3.14F;
                string yargbh = "String";
                double yargz = 2952935923592359235;
                int yarga = 32;

                overload(yargh);
                overload(yarghb);
                overload(yargbh);
                overload(yarga);
                overload(yargz);
            }
            finally
            {
                Console.WriteLine("\nDone with overloads!\n\n\n");
                overridingex();
            }
        }
        abstract class shapes
        {
            internal int count = 0; //Internal
            abstract public int Area();
            public void addct()
            {
                count++;
            }
            public int getct()
            {
                return count;
            }
        }
        class Square : shapes
        {
            int side = 0;
            public Square(int x)
            {
                addct();
                side = x;
            }
            public override int Area()
            {
                return side * side;
            }
        }
        private static void overridingex()
        {
            try
            {
                Square sqobj = new Square(15);
                Console.WriteLine("\nArea of square: {0}", sqobj.Area());
            }
            finally
            {
                derivedclassexample();
            }
        }

        //Here's a derived class
        class Triangle : shapes
        {
            protected int tribase = 15; //Protected
            protected internal int triheight = 5; //Protected internal
            public Triangle(int b, int h)
            {
                addct();
                tribase = b;
                triheight = h;
            }
            public override int Area() //Public
            {
                return (tribase * triheight) / 2;
            }
        }
        private static void derivedclassexample() //Private
        {
            try
            {
                Triangle triobj = new Triangle(13, 5);
                Console.WriteLine("\n\nArea of derived Triangle obj: {0}", triobj.Area());
                Console.WriteLine("Triangle object count is now {0}.", triobj.getct());
            }
            finally
            {
                Console.WriteLine("\n\n\nExamples for access modifiers:\n Protected = Line 100\nProtected Internal = 101\nPublic = 108\nPrivate = 113\nInternal = 60\n\n\n");
                sealedclass();
            }
        }
        //interface
        interface shapes2rect
        {
            int w
            {
                get;
                set;
            }
            int h
            {
                get;
                set;
            }
        }
        sealed class Rectangle : shapes2rect
        {
            private int _w;
            private int _h;
            public Rectangle(int w, int h)
            {
                _w = w;
                _h = h;
            }
            public int w
            {
                get
                {
                    return w;
                }
                set
                {
                    _w = value;
                }
            }
            public int h
            {
                get
                {
                    return h;
                }
                set
                {
                    _h = value;
                }
            }
            public int Area()
            {
                throw new NotImplementedException();
            }
        }

        private static void sealedclass()
        {
            try
            {
                Console.WriteLine("See line 128 for a sealed rectangle class ex.\n Additionally an example of using the interface class constructor is available there as well.");
            }
            catch (NotImplementedException)
            {
                Console.WriteLine("Error: Area method of rectangle obj not implemented");
                string errorFile = @"C:\Error.txt";
                using (StreamWriter writer = new StreamWriter(errorFile, true))
                {
                    writer.WriteLine("Exception Not Implemented (line 175) thrown on" + DateTime.Now.ToString());
                }
            }
            finally
            {
                Console.WriteLine("\n\n Line 184 has an example of 'try catch finally' programming including error logging.");
                valuevsref();
            }
        }
        [Serializable()]
        public struct payload//This is what a payload for an RF radio would look like, and it's a Value Type example.
        {//These are stored in stack memory
            public long data;
            public long key;
            public payload(long arg1, long arg2)
            {
                data = arg1;
                key = arg2;
            }
        }
        public class payload2//This is a payload class, which is a reference, and is stored in heap memory.
        {
            public long data;
            public long key;
        }
        private static void valuevsref()
        {
            Console.WriteLine("See lines 200-209 for Value Type vs Reference Type example.");
            usekeywordthis();
        }
        [Serializable()]
        public class usethis
        {
            internal int count = 0;
            public string alias = "";
            public usethis(string alias)
            {
                this.alias = alias;
                count++;
            }
        }
        private static void usekeywordthis()
        {
            Console.WriteLine("\nOk, so... 'This' usage is at line 222.");
            usethis demo = new usethis("Demo Instance");
            Console.WriteLine("{0}", demo.alias); //Should return the alias set with the constructor above 'demo instance'
            delegatesEx();
        }

        //Delegates
        public delegate void cwlt(string message);
        public static void ConsoleMsgTime(string message)
        {
            Console.WriteLine("{0} - {1}", message, DateTime.Now.ToString("h:mm:ss tt"));
        }
        //Multi deligate
        public delegate void Deltest(string str);

        static void timePrint(string str)
        {
            Console.WriteLine("{0} - {1}", str, DateTime.Now.ToString("h:mm:ss tt"));
        }
        static void datePrint(string str)
        {
            Console.WriteLine("{0} - {1}", str, DateTime.Today.ToString("D"));
        }

        public static void delegatesEx()
        {
            cwlt handler = ConsoleMsgTime;
            handler("\n\n\nHello world, with a timestamp.");
            
            Deltest timeprint, dateprint, multiprint;

            timeprint = timePrint;
            dateprint = datePrint;
            multiprint = timeprint + dateprint;

            timeprint("\nHello world with a timestamp");
            dateprint("\nHello world with a datestamp");
            multiprint("\nHello world with both.");
  

            donullabletypes();
        }

        private static void donullabletypes()
        {
            int? num = null;
            try
            {
                Console.WriteLine("num is", num.Value);
            }
            catch(System.InvalidOperationException e)
            {
                Console.WriteLine(e.Message);
            }

            doenumerables();
        }

        private static void doenumerables()
        {
            List<string> thingsilike = new List<string>();

            thingsilike.Add("Panda Express");
            thingsilike.Add("All Wheel Drive");
            thingsilike.Add("Summer");
            thingsilike.Add("The Color Blue");
            thingsilike.Add("Airplanes");

            IEnumerable<string> senum = (IEnumerable<string>)thingsilike;

            foreach(string s in senum)
            {
                Console.WriteLine(s);
            }

            doBLOBserializationex();
           
        }
        //Write to binary file, from stackoverflow
        public static void WriteToBinaryFile<T>(string filePath, T objectToWrite, bool append = true)
        {
            try
            {
                using (Stream stream = File.Open(filePath, append ? FileMode.Append : FileMode.Create))
                {
                    var binaryFormatter = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
                    binaryFormatter.Serialize(stream, objectToWrite);
                }
            }
            catch(UnauthorizedAccessException)
            {
                Console.WriteLine("Need to manually create a file at C:\\ called blob.txt");
            }
        }
        public static T ReadFromBinaryFile<T>(string filePath)
        {
            using (Stream stream = File.Open(filePath, FileMode.Open))
            {
                var binaryFormatter = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
                return (T)binaryFormatter.Deserialize(stream);
            }
        }
        private static void doBLOBserializationex()
        {
            payload demo = new payload(01,10);
            usethis demo2 = new C_Sharp_Drills.drills.usethis("Blob Serialization Worked?!");

            string fileLocation = "C:\\Users\\Chris.SERVER0\\Documents\\blob.txt";

            // Write the contents of the variable someClass to a file.
            WriteToBinaryFile<usethis>(fileLocation, demo2);
                            // Read the file contents back into a variable.
            usethis newobj = ReadFromBinaryFile<usethis>(fileLocation);
            Console.WriteLine("\n\n\n{0}",newobj.alias);
            Console.WriteLine("If the above message reflects the new objects alias created above, then the serialization / deserialization worked.");
            File.WriteAllText(fileLocation, String.Empty);//Empty the file.
            Console.WriteLine("Press Any Key to Quit");
            Console.Read(); //Keeps console open.
        }
    }
}